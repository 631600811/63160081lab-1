import 'dart:io';

void main() {
  print('Please input sentence : ');
  String? input = (stdin.readLineSync()!);
  List<String> words = [];
  String word = '';
  List<String> checkwords = [];
  String checkword = '';
  int count;

  for (int i = 0; i < input.length; i++) {
    //input[i];
    if (input.toUpperCase()[i] != ' ') {
      word = word + input[i];
      checkword = input.toUpperCase()[i];
    } else if (input.toUpperCase()[i] == ' ') {
      words.add(word);
      checkwords.add(checkword);
      word = '';
    }
  }

  for (int i = 0; i < checkwords.length; i++) {
    int count = 0;
    for (int j = i + 1; j < checkwords.length; j++) {
      if (checkwords[i] == checkwords[j]) {
        count++;
        words[j] = '0';
      }
    }
    if (count > 1 && words[i] != '0') {
      stdout.write(words[i]);
    }
  }
}
